package net.larboard.lib.error.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import net.larboard.lib.error.exception.impl.ValidationException;

import java.util.List;

@Getter
@Setter
public class ValidationErrorMessage extends ErrorMessage {
    @JsonProperty("causes")
    private List<ValidationException.ValidationError> causes;

    public ValidationErrorMessage(ValidationException ex, boolean verbose) {
        super(ex, verbose);

        this.causes = ex.getErrors();
    }
}
