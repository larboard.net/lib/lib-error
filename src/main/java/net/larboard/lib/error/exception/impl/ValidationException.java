package net.larboard.lib.error.exception.impl;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.larboard.lib.error.exception.CoreRuntimeException;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class ValidationException extends CoreRuntimeException {
    @Getter
    private final List<ValidationError> errors = new ArrayList<>();

    public final void addError(ValidationError error) {
        errors.add(error);
    }

    @Getter
    private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST; // don't make static: breaks inherited abstract getter

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(List<ValidationError> errors) {
        super("validation failed");

        this.errors.addAll(errors);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    @Getter
    @Setter
    public static final class ValidationError {
        public static ValidationError of(@NonNull final String fieldIdentifier, final String errorCode, final String errorMessage) {
            ValidationError instance = new ValidationError();

            instance.fieldIdentifier = fieldIdentifier;
            instance.errorCode = errorCode;
            instance.errorMessage = errorMessage;

            return instance;
        }

        private String fieldIdentifier;

        private String errorCode;

        private String errorMessage;
    }
}
