package net.larboard.lib.error.exception.controller;

import lombok.extern.slf4j.Slf4j;
import net.larboard.lib.error.exception.CoreException;
import net.larboard.lib.error.exception.CoreRuntimeException;
import net.larboard.lib.error.exception.ErrorMessage;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

@Slf4j
@ControllerAdvice
public abstract class SimpleExceptionHandlerController {
    @Value("${errorHandling.verboseException:false}")
    private boolean verboseException;

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleACoreException(CoreException ex) {
        log.error("{} thrown, cause of: {}", ex.getClass().getName(), ex.getMessage());

        return createErrorResponse(ex.getHttpStatus(), ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleACoreRuntimeException(CoreRuntimeException ex) {
        log.error("{} thrown, cause of: {}", ex.getClass().getName(), ex.getMessage());

        return createErrorResponse(ex.getHttpStatus(), ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleRequestRejectedException(RequestRejectedException ex) {
        log.error("RequestRejectedException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.BAD_REQUEST, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleAccessDeniedException(AccessDeniedException ex) {
        log.error("AccessDeniedException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.FORBIDDEN, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleAuthenticationServiceException(AuthenticationServiceException ex) {
        log.error("AuthenticationServiceException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleSessionAuthenticationException(SessionAuthenticationException ex) {
        log.error("SessionAuthenticationException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.TOO_MANY_REQUESTS, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleAuthenticationCredentialsNotFoundException(AuthenticationCredentialsNotFoundException ex) {
        log.error("AuthenticationCredentialsNotFoundException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleLockedException(LockedException ex) {
        log.error("LockedException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.LOCKED, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleAuthenticationException(AuthenticationException ex) {
        log.error("AuthenticationException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.UNAUTHORIZED, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        log.error("HttpMessageNotReadableException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleTypeMismatchException(TypeMismatchException ex) {
        log.error("TypeMismatchException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.BAD_REQUEST, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
        log.error("HttpRequestMethodNotSupportedException thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.BAD_REQUEST, ex);
    }

    @ExceptionHandler
    protected ResponseEntity<ErrorMessage> handleAnyException(Exception ex) {
        log.error("Exception thrown, cause of: {}", ex.getMessage());

        return createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    }

    private ResponseEntity<ErrorMessage> createErrorResponse(HttpStatus httpStatus, Exception ex) {
        return createErrorResponse(httpStatus, httpStatus.value(), ex);
    }

    private ResponseEntity<ErrorMessage> createErrorResponse(HttpStatus httpStatus, int errorCode, Exception ex) {
        log.debug(getStackTrace(ex));

        ErrorMessage message = new ErrorMessage(ex, verboseException);
        message.setErrorCode(errorCode);

        return new ResponseEntity<>(message, httpStatus);
    }

    private static String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
}
